/*
See LICENSE folder for this sample’s licensing information.

Abstract:
A class representing a local server that vends exposure data.
*/

import Foundation
import ExposureNotification
import CommonCrypto
import ZIPFoundation
import CryptoKit

struct CodableDiagnosisKey: Codable, Equatable {
    let keyData: Data
    let rollingPeriod: ENIntervalNumber
    let rollingStartNumber: ENIntervalNumber
    let transmissionRiskLevel: ENRiskLevel
}

struct CodableExposureConfiguration: Codable {
    // API V2 Keys
    let immediateDurationWeight: Double
    let nearDurationWeight: Double
    let mediumDurationWeight: Double
    let otherDurationWeight: Double
    let infectiousnessForDaysSinceOnsetOfSymptoms: [String: Int]
    let infectiousnessStandardWeight: Double
    let infectiousnessHighWeight: Double
    let reportTypeConfirmedTestWeight: Double
    let reportTypeConfirmedClinicalDiagnosisWeight: Double
    let reportTypeSelfReportedWeight: Double
    let reportTypeRecursiveWeight: Double
    let reportTypeNoneMap: Int
    // API V1 Keys
    let minimumRiskScore: ENRiskScore
    let attenuationDurationThresholds: [Int]
    let attenuationLevelValues: [ENRiskLevelValue]
    let daysSinceLastExposureLevelValues: [ENRiskLevelValue]
    let durationLevelValues: [ENRiskLevelValue]
    let transmissionRiskLevelValues: [ENRiskLevelValue]
    //Threshold
    let confirmedTestPerDaySumERVThreshold: Double
    let clinicalDiagnosisPerDaySumERVThreshold: Double
    let selfReportPerDaySumERVThreshold: Double
    let recursivePerDaySumERVThreshold: Double
    let perDaySumERVThreshold: Double
    let perDayMaxERVThreshold: Double
}

// Replace this class with your own class that communicates with your server.
class Server {
    
    static let shared = Server();
    var BASE_URL = "https://apiserver-4wn4flxw4q-uc.a.run.app";
    var UPLOAD_URL = "https://exposure-zsccohrqra-uc.a.run.app/v1/publish"
    // HKG_API
//    var API_KEY = "HRkBAJE8XUrqgRQQ9yqFAbFwzjnpOAHs4QSU6Qj8OyHvQCoOMuO_VoCHImiq8QyTC1IDEXb2awGdoLQ1llErxQ.3.GzircK8fw847EFBrD0dfiv4f4NMgzQGJS_ezDH6xQd7xj54y84NBSRNC3VW6FXQTXvbgpsWze1fPi_hhbwXS2A";
    // CNHK_API
    var API_KEY = "yUv13NM1Yac462PopbG2gJcJBOz4hbsTxVh-RbBj07M-nxzIniydytxnosd7hy7nLtpXlv_C0bEv32bUwQkajw.6.z4NtQrp0VLTfkKWdyTRBWexZzj0YWUuy2x1JqkBFaHA8dKuCNfBe-p6acI1BqBzq4eg24WbJiAMPFS1U0cp-qg"
    
    var SERVER_FILES_BASE_URL = "https://storage.googleapis.com/exposure-notification-export-txasb/";
    var SERVER_FILES_INDEX = "https://storage.googleapis.com/exposure-notification-export-txasb/lhs/index.txt";
    var PUBLIC_KEY = "8yfpIUC8q/G+T2kAn7rzwOR8nYPjlqSpW6GlMO0gFMxne9zBEUMJO1Cf8diNzr03oj7w//HAe/ExB6P606CtUQ==";
    var CONFIG_URL = "https://storage.googleapis.com/exposure-notification-export-txasb/config/gaen_config.json";
    // For testing purposes, this object stores all of the TEKs it receives locally on device
    // In a real implementation, these would be stored on a remote server
    @Persisted(userDefaultsKey: "diagnosisKeys", notificationName: .init("ServerDiagnosisKeysDidChange"), defaultValue: [])
    var diagnosisKeys: [CodableDiagnosisKey]
    func postDiagnosisKeys(_ diagnosisKeys: [ENTemporaryExposureKey], token: String, symptomDate: UInt32, completion: @escaping (Error?) -> Void) {
        
        // In a real implementation, these keys would be uploaded with URLSession instead of being saved here.
        // Your server needs to handle de-duplicating keys.
     
        /*for codableDiagnosisKey in codableDiagnosisKeys where !self.diagnosisKeys.contains(codableDiagnosisKey) {
            self.diagnosisKeys.append(codableDiagnosisKey)
        }*/
        
//        verifyCode(code: "97925685", diagnosisKeys: diagnosisKeys);
        
        let key = self.randomString(length: 32);
        var tekKey = "";
        var tekList: [ENTek] = [];
        var combineTekList: [String] = [];
        for diagnosisKey in diagnosisKeys {
            if tekKey != "" { tekKey += "," } ;
            var combinedTek = diagnosisKey.keyData.base64EncodedString()
                + "." + String(diagnosisKey.rollingStartNumber) + "." +
                String(diagnosisKey.rollingPeriod) + "." +
                String(diagnosisKey.transmissionRiskLevel);

            combineTekList.append(combinedTek);
            tekList.append(ENTek(key: diagnosisKey.keyData.base64EncodedString(),
                                 rollingPeriod: diagnosisKey.rollingPeriod,
                                 rollingStartNumber: diagnosisKey.rollingStartNumber,
                                 transmissionRisk: diagnosisKey.transmissionRiskLevel))
        }
        
        combineTekList.sort();
        tekKey = combineTekList.joined(separator: ",")
        print ("diagnosisKey " + tekKey);
            
        let ekeyhmac = self.hmac(tekKey: tekKey, key: key);
        print("ekeyhmac: ", ekeyhmac);
        print("hmacKey: ", key);
        print("tekKeys: ", tekList);
        print("token: ", token);
        print("symptomDate: ", symptomDate);
        getCertificate(token: token, ekeyhmac: ekeyhmac, hmacKey: key, tekKeys: tekList, symptomDate: symptomDate) { error in
            completion(error)
        }
//        completion(nil)
    }
    
    func hmac(tekKey: String, key: String) -> String {
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
        CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA256), key, key.count, tekKey, tekKey.count, &digest)
        let data = Data(_: digest)
        return data.base64EncodedString();
//       return data.map { String(format: "%02hhx", $0) }.joined()
    }
    
    func verifyCode(code: String, completion: @escaping (Result<VerifyCodeResult, Error>) -> Void) {
        let url = URL(string: BASE_URL+"/api/verify")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(API_KEY, forHTTPHeaderField: "x-api-key")
        print("[verifyCode] code:", code);
        let uploadData = [
           "code": code,
           "accept": ["confirmed", "likely", "negative"],
           "padding": randomInt()
        ] as [String : Any]
        do {
            let json = try JSONSerialization.data(withJSONObject: uploadData)
           
            let task = URLSession.shared.uploadTask(with: request, from: json) { responseData, response, error in
                // Check on some response headers (if it's HTTP)
                do {
                if let httpResponse = response as? HTTPURLResponse {
                    switch httpResponse.statusCode {
                        case 200..<300:
                            print("Success")
                        case 400..<500:
                            print("Request error")
                        case 500..<600:
                            print("Server error")
                        case let otherCode:
                            print("Other code: \(otherCode)")
                    }
                }
                
                // Do something with the response data
                if let responseData = responseData,
                    let responseString = String(data: responseData, encoding: String.Encoding.utf8) {
                        print("[verifyCode] Server Response:")
                        print(responseString);
                       
                        let json = try JSONDecoder().decode(VerifyCodeResult.self, from: responseData);
                        completion(.success(json))
                    }
                
                    // Do something with the error
                    if let error = error {
                        print(error.localizedDescription)
                        completion(.failure(error))
                    }
                } catch {
                  print("[verifyCode] response - JSON serialization failed: ", error)
                  completion(.failure(error))
                }
            }
            

            task.resume();
        } catch {
          print("[verifyCode] JSON serialization failed: ", error)
          completion(.failure(error))
        }
       
    }
    
    func getCertificate(token: String, ekeyhmac:String, hmacKey: String, tekKeys: [ENTek], symptomDate: UInt32, completion: @escaping (Error?) -> Void) {
        let url = URL(string: BASE_URL+"/api/certificate")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(API_KEY, forHTTPHeaderField: "x-api-key")
        
        let uploadData = [
           "token": token,
           "ekeyhmac": ekeyhmac,
           "padding": randomInt()
        ] as [String : Any]
        do {
            let json = try JSONSerialization.data(withJSONObject: uploadData)
            print(String(data: json, encoding: .utf8)!)
            let task = URLSession.shared.uploadTask(with: request, from: json) { responseData, response, error in
                // Check on some response headers (if it's HTTP)
                do {
                    if let httpResponse = response as? HTTPURLResponse {
                        switch httpResponse.statusCode {
                            case 200..<300:
                                print("[getCertificate] Success")
                            case 400..<500:
                                print("[getCertificate] Request error")
                            case 500..<600:
                                print("[getCertificate] Server error")
                            case let otherCode:
                                print("[getCertificate] Other code: \(otherCode)")
                        }
                    }
                    
                    // Do something with the response data
                    if let
                        responseData = responseData,
                        let responseString = String(data: responseData, encoding: String.Encoding.utf8) {
                        print("[getCertificate] Server Response:")
                        print(responseString)
                        
                        let json = try JSONDecoder().decode(VerificationCertResult.self, from: responseData)
                       
                        if (json.certificate != nil) {
                            var revisionToken = LocalStore.shared.revisionToken;
                            if revisionToken == nil {
                                revisionToken = self.randomString(length: 32)
                            }
                            print("revisionToken", revisionToken)
                            let uploadData = UploadData(
                                temporaryExposureKeys: tekKeys,
                                healthAuthorityID: "chp",
                                verificationPayload: json.certificate,
                                hmacKey: Data(String(hmacKey).utf8).base64EncodedString(),
                                symptomOnsetInterval: symptomDate,
                                traveler: false,
                                revisionToken: revisionToken!,
                                padding: self.randomInt()
                            );
                           
                            self.uploadTek(uploadData: uploadData){error in
                                completion(error)
                            }
                        } else {
                            completion(NSError(domain:"", code:401, userInfo:[ NSLocalizedDescriptionKey: json.error]))
                        }
                    }
                    
                    // Do something with the error
                    if let error = error {
                        print(error.localizedDescription)
                    }
                } catch {
                    print("[getCertificate] response - JSON serialization failed: ", error)
                    completion(error)
                }
            }
            

            task.resume();
        } catch {
            print("[getCertificate] JSON serialization failed: ", error)
            completion(error)
        }
       
    }
    
    func uploadTek(uploadData: UploadData, completion: @escaping (Error?) -> Void) {
        let url = URL(string: UPLOAD_URL)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(API_KEY, forHTTPHeaderField: "x-api-key")
        
        do {
            let json = try JSONEncoder().encode(uploadData);
            print("[uploadTek] UploadData request data: ");
            print(String(data: json, encoding: .utf8)!)
            let task = URLSession.shared.uploadTask(with: request, from: json) { responseData, response, error in
                // Check on some response headers (if it's HTTP)
                do {
                if let httpResponse = response as? HTTPURLResponse {
                    switch httpResponse.statusCode {
                        case 200..<300:
                            print("Success")
                        case 400..<500:
                            print("Request error")
                        case 500..<600:
                            print("Server error")
                        case let otherCode:
                            print("Other code: \(otherCode)")
                    }
                }
                
                // Do something with the response data
                if let responseData = responseData,
                    let responseString = String(data: responseData, encoding: String.Encoding.utf8) {
                        print("[uploadTek] Server Response:")
                        print(responseString);
                        let json = try JSONDecoder().decode(UploadResult.self, from: responseData)
                        if ((json.error) != nil) {
                            completion(NSError(domain:"", code:401, userInfo:[ NSLocalizedDescriptionKey: json.error]))
                        } else {
                            print("[uploadTek] save revision token:", json.revisionToken);
                            LocalStore.shared.revisionToken = json.revisionToken;
                            completion(nil)
                        }
                    }
                   
                    // Do something with the error
                    if let error = error {
                        completion(error)
                        print(error.localizedDescription)
                    }
                } catch {
                    print("[uploadTek] response - JSON serialization failed: ", error)
                    completion(error)
                }
            }
            

            task.resume();
        } catch {
            print("[uploadTek] JSON serialization failed: ", error)
            completion(error)
        }
    }
    
    func getDiagnosisKeyFileURLs(startingAt index: Int,  completion: @escaping (Result<[URL], Error>) -> Void) {
        
        // In a real implementation, these URLs would be retrieved from a server with URLSession
        // This sample only returns one placeholder URL, because the diagnosis key file is generated in each call to downloadDiagnosisKeyFile
        let url = URL(string: SERVER_FILES_INDEX)!

        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            print(String(data: data, encoding: .utf8)!)
            
            if let httpResponse = response as? HTTPURLResponse {
                switch httpResponse.statusCode {
                    case 200..<300:
                        print("Success")
                    case 400..<500:
                        completion(.failure(NSError(domain:"", code:httpResponse.statusCode, userInfo:[ NSLocalizedDescriptionKey: "Request Error"])))
                    case 500..<600:
                        completion(.failure(NSError(domain:"", code:httpResponse.statusCode, userInfo:[ NSLocalizedDescriptionKey: "Server Error"])))
                    case let otherCode:
                        print("Other code: \(otherCode)")
                }
            }
                
            let result = String(data: data, encoding: .utf8);
            let zipFileList = result!.components(separatedBy: NSCharacterSet.newlines).filter{!$0.isEmpty}

            var remoteURLs = [URL]();
            var currIndex = 0;
            var downloadFile = false;
            var downloadIndex = 0;
            print ("install date", LocalStore.shared.appInstallTs);
            for zipFile in zipFileList {
//                if (LocalStore.shared.nextDiagnosisKeyFileIndex == 0) {
                let fileName = URL(string: zipFile)?.lastPathComponent;
                if (LocalStore.shared.nextDiagnosisKeyFile == nil) {
                    let fileNameArr = fileName!.components(separatedBy: "-");
                    let startTs = fileNameArr[0];
                    let endTs = fileNameArr[1];
                    
                    if LocalStore.shared.appInstallTs >= Double(startTs)! && LocalStore.shared.appInstallTs <= Double(endTs)! {
                        downloadFile = true
                        LocalStore.shared.tmpNextDiagnosisKeyFile = zipFile;
                        downloadIndex = currIndex
                    }
                } else if (LocalStore.shared.nextDiagnosisKeyFile == zipFile) {
                    downloadFile = true
                    downloadIndex = currIndex + 1
                }
                print ("LocalStore.shared.nextDiagnosisKeyFile ", LocalStore.shared.nextDiagnosisKeyFile);
                print ("downloadFile ", downloadFile);
                print ("currIndex ", currIndex);
                print ("downloadIndex ", downloadIndex);
                if (downloadFile && currIndex >= downloadIndex) {
                    remoteURLs.append(URL(string: self.SERVER_FILES_BASE_URL + zipFile)!);
                    LocalStore.shared.tmpNextDiagnosisKeyFile = zipFile;
                }
                currIndex+=1;
            }
            print("remoteURLs: ", remoteURLs);
            completion(.success(remoteURLs))
        }

        task.resume()
        
//        let remoteURLs = [URL(string: "/url/to/export\(index)")!]
//
//        completion(.success(Array(remoteURLs[min(index, remoteURLs.count)...])))
    }
    
    // In a real implementation, this would be kept secret on your server
    // This is a sample private key - you will need to generate your own public-private key pair and share the public key with Apple
    // NOTE: The backslash on the end of the first line is not part of the key
    static let privateKeyECData = Data(base64Encoded: """
    MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgKJNe9P8hzcbVkoOYM4hJFkLERNKvtC8B40Y/BNpfxMeh\
    RANCAASfuKEs4Z9gHY23AtuMv1PvDcp4Uiz6lTbA/p77if0yO2nXBL7th8TUbdHOsUridfBZ09JqNQYKtaU9BalkyodM
    """)!
    
    // The URL passed to the completion is the local URL of the downloaded diagnosis key file
    func downloadDiagnosisKeyFile(at remoteURL: URL, completion: @escaping (Result<[URL], Error>) -> Void) {
        do {

            let request = URLRequest(url: remoteURL);
            let cachesDirectory:URL =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
            let splitUrl = remoteURL.path.components(separatedBy: "/").filter{!$0.isEmpty};
            let fileName = splitUrl[splitUrl.count - 1];
            let destinationFileUrl = cachesDirectory.appendingPathComponent(fileName);
            let folderName = fileName.replacingOccurrences(of: ".zip", with: "")
            let unzipDest = cachesDirectory.appendingPathComponent(folderName);
            print("destinationFileUrl", destinationFileUrl);
            print("unzipDest", unzipDest);
            let task = URLSession.shared.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        print("Successfully downloaded. Status code: \(statusCode)")
                    }
                    
                    do {
                        try? FileManager.default.removeItem(at: destinationFileUrl);
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl);
                        
                        do {
                            if FileManager.default.fileExists(atPath: unzipDest.path) {
                                print("File directory exists");
                            } else {
                                print("Directory does not exists, create directory");
                                try FileManager.default.createDirectory(at: unzipDest, withIntermediateDirectories: true, attributes: nil)
                            }
                            
                            let localBinURL = unzipDest.appendingPathComponent("export.bin");
                            let localSigURL = unzipDest.appendingPathComponent("export.sig");
                            try? FileManager.default.removeItem(at: localBinURL);
                            try? FileManager.default.removeItem(at: localSigURL);
                            try FileManager.default.unzipItem(at: destinationFileUrl, to: unzipDest)
                        
                            if #available(iOS 13.0, *) {
                                if (self.verifySignature(destinationFileUrl: destinationFileUrl)) {
                                    completion(.success([localBinURL, localSigURL]))
                                } else {
                                    completion(.failure(NSError(domain:"", code:401, userInfo:[ NSLocalizedDescriptionKey: "File signature verification failed"])))
                                }
                            } else {
                                // Fallback on earlier versions
                                completion(.success([localBinURL, localSigURL]))
                            }
                        } catch {
                            print("Extraction of ZIP archive failed with error:\(error)")
                        }
                        
                    } catch (let writeError) {
                        print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    }
                    
                } else {
                    print("Error took place while downloading a file. Error description: %@", error?.localizedDescription);
                }
            }
            task.resume();
            
//            completion(.success([localBinURL, localSigURL]))
        } catch {
            completion(.failure(error))
        }
    }
    
    func deleteDiagnosisKeyFile(at localURLs: [URL]) throws {
        for localURL in localURLs {
            try FileManager.default.removeItem(at: localURL)
        }
    }
    
    func getExposureConfiguration(completion: @escaping (Result<ENConfig, Error>) -> Void) {
        print("[getExposureConfiguration] start");
        let url = URL(string: CONFIG_URL)!

        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            print(String(data: data, encoding: .utf8)!)
//            let dataFromServer = String(data: data, encoding: .utf8)!
//        let dataFromServer = """
//        {
//        "immediateDurationWeight":150,
//        "nearDurationWeight":100,
//        "mediumDurationWeight":40,
//        "otherDurationWeight":0,
//        "infectiousnessForDaysSinceOnsetOfSymptoms":{
//            "unknown":1,
//            "-14":0,
//            "-13":0,
//            "-12":0,
//            "-11":0,
//            "-10":0,
//            "-9":0,
//            "-8":0,
//            "-7":0,
//            "-6":0,
//            "-5":0,
//            "-4":0,
//            "-3":1,
//            "-2":2,
//            "-1":2,
//            "0":2,
//            "1":2,
//            "2":2,
//            "3":2,
//            "4":1,
//            "5":0,
//            "6":0,
//            "7":0,
//            "8":0,
//            "9":0,
//            "10":0,
//            "11":0,
//            "12":0,
//            "13":0,
//            "14":0
//        },
//        "infectiousnessStandardWeight":30,
//        "infectiousnessHighWeight":100,
//        "reportTypeConfirmedTestWeight":100,
//        "reportTypeConfirmedClinicalDiagnosisWeight":100,
//        "reportTypeSelfReportedWeight":100,
//        "reportTypeRecursiveWeight":100,
//        "reportTypeNoneMap":1,
//        "minimumRiskScore":0,
//        "attenuationDurationThresholds":[55, 63, 70],
//        "attenuationLevelValues":[1, 2, 3, 4, 5, 6, 7, 8],
//        "daysSinceLastExposureLevelValues":[1, 2, 3, 4, 5, 6, 7, 8],
//        "durationLevelValues":[1, 2, 3, 4, 5, 6, 7, 8],
//        "transmissionRiskLevelValues":[1, 2, 3, 4, 5, 6, 7, 8]
//        }
//        """.data(using: .utf8)!
//        let dataFromServer = """
//         {
//            "immediateDurationWeight":100,
//            "nearDurationWeight":100,
//            "mediumDurationWeight":100,
//            "otherDurationWeight":100,
//            "infectiousnessForDaysSinceOnsetOfSymptoms":{
//                "unknown":1,
//                "-14":1,
//                "-13":1,
//                "-12":1,
//                "-11":1,
//                "-10":1,
//                "-9":1,
//                "-8":1,
//                "-7":1,
//                "-6":1,
//                "-5":1,
//                "-4":1,
//                "-3":1,
//                "-2":1,
//                "-1":1,
//                "0":1,
//                "1":1,
//                "2":1,
//                "3":1,
//                "4":1,
//                "5":1,
//                "6":1,
//                "7":1,
//                "8":1,
//                "9":1,
//                "10":1,
//                "11":1,
//                "12":1,
//                "13":1,
//                "14":1
//            },
//            "infectiousnessStandardWeight":100,
//            "infectiousnessHighWeight":100,
//            "reportTypeConfirmedTestWeight":100,
//            "reportTypeConfirmedClinicalDiagnosisWeight":100,
//            "reportTypeSelfReportedWeight":100,
//            "reportTypeRecursiveWeight":100,
//            "reportTypeNoneMap":1,
//            "minimumRiskScore":0,
//            "attenuationDurationThresholds":[50, 70],
//            "attenuationLevelValues":[1, 2, 3, 4, 5, 6, 7, 8],
//            "daysSinceLastExposureLevelValues":[1, 2, 3, 4, 5, 6, 7, 8],
//            "durationLevelValues":[1, 2, 3, 4, 5, 6, 7, 8],
//            "transmissionRiskLevelValues":[1, 2, 3, 4, 5, 6, 7, 8]
//            }
//         """.data(using: .utf8)!
        
            do {
                let codableExposureConfiguration = try JSONDecoder().decode(CodableExposureConfiguration.self, from: data)
                let exposureConfiguration = ENExposureConfiguration()
                if ENManagerIsAvailable() {
                    exposureConfiguration.immediateDurationWeight = codableExposureConfiguration.immediateDurationWeight
                    exposureConfiguration.nearDurationWeight = codableExposureConfiguration.nearDurationWeight
                    exposureConfiguration.mediumDurationWeight = codableExposureConfiguration.mediumDurationWeight
                    exposureConfiguration.otherDurationWeight = codableExposureConfiguration.otherDurationWeight
                    var infectiousnessForDaysSinceOnsetOfSymptoms = [Int: Int]()
                    for (stringDay, infectiousness) in codableExposureConfiguration.infectiousnessForDaysSinceOnsetOfSymptoms {
                        if stringDay == "unknown" {
                            if #available(iOS 14.0, *) {
                                infectiousnessForDaysSinceOnsetOfSymptoms[ENDaysSinceOnsetOfSymptomsUnknown] = infectiousness
                            } else {
                                // ENDaysSinceOnsetOfSymptomsUnknown is not available
                                // in earlier versions of iOS; use an equivalent value
                                infectiousnessForDaysSinceOnsetOfSymptoms[NSIntegerMax] = infectiousness
                            }
                        } else if let day = Int(stringDay) {
                            infectiousnessForDaysSinceOnsetOfSymptoms[day] = infectiousness
                        }
                    }
                    exposureConfiguration.infectiousnessForDaysSinceOnsetOfSymptoms = infectiousnessForDaysSinceOnsetOfSymptoms as [NSNumber: NSNumber]
                    exposureConfiguration.infectiousnessStandardWeight = codableExposureConfiguration.infectiousnessStandardWeight
                    exposureConfiguration.infectiousnessHighWeight = codableExposureConfiguration.infectiousnessHighWeight
                    exposureConfiguration.reportTypeConfirmedTestWeight = codableExposureConfiguration.reportTypeConfirmedTestWeight
                    exposureConfiguration.reportTypeConfirmedClinicalDiagnosisWeight = codableExposureConfiguration.reportTypeConfirmedClinicalDiagnosisWeight
                    exposureConfiguration.reportTypeSelfReportedWeight = codableExposureConfiguration.reportTypeSelfReportedWeight
                    exposureConfiguration.reportTypeRecursiveWeight = codableExposureConfiguration.reportTypeRecursiveWeight
                    exposureConfiguration.attenuationDurationThresholds =
                        codableExposureConfiguration.attenuationDurationThresholds  as [NSNumber]
                    if let reportTypeNoneMap = ENDiagnosisReportType(rawValue: UInt32(codableExposureConfiguration.reportTypeNoneMap)) {
                        exposureConfiguration.reportTypeNoneMap = reportTypeNoneMap
                    }
                }
                exposureConfiguration.minimumRiskScore = codableExposureConfiguration.minimumRiskScore
                exposureConfiguration.attenuationLevelValues = codableExposureConfiguration.attenuationLevelValues as [NSNumber]
                exposureConfiguration.daysSinceLastExposureLevelValues = codableExposureConfiguration.daysSinceLastExposureLevelValues as [NSNumber]
                exposureConfiguration.durationLevelValues = codableExposureConfiguration.durationLevelValues as [NSNumber]
                exposureConfiguration.transmissionRiskLevelValues = codableExposureConfiguration.transmissionRiskLevelValues as [NSNumber]
                exposureConfiguration.metadata = ["attenuationDurationThresholds": codableExposureConfiguration.attenuationDurationThresholds]
                print("[getExposureConfiguration] complete success", exposureConfiguration);
                
                let threshold = ENThreshold();
                threshold.confirmedTestPerDaySumERVThreshold = codableExposureConfiguration.confirmedTestPerDaySumERVThreshold;
                threshold.clinicalDiagnosisPerDaySumERVThreshold = codableExposureConfiguration.clinicalDiagnosisPerDaySumERVThreshold;
                threshold.selfReportPerDaySumERVThreshold = codableExposureConfiguration.selfReportPerDaySumERVThreshold;
                threshold.recursivePerDaySumERVThreshold = codableExposureConfiguration.recursivePerDaySumERVThreshold;
                threshold.perDaySumERVThreshold = codableExposureConfiguration.perDaySumERVThreshold;
                threshold.perDayMaxERVThreshold = codableExposureConfiguration.perDayMaxERVThreshold;
                
                let enConfig = ENConfig();
                enConfig.threshold = threshold;
                enConfig.exposureConfig = exposureConfiguration;
                completion(.success(enConfig))
            } catch {
                print("[getExposureConfiguration] complete error");
                completion(.failure(error))
            }
        }
        task.resume();
    }
    
    func verifyUniqueTestIdentifier(_ identifier: String, completion: @escaping (Result<VerifyCodeResult, Error>) -> Void) {
        
        // In a real implementation, this identifer would be validated on a server
        verifyCode(code: identifier) { result in
            switch result {
            case let .success(response):
                completion(.success(response))
            case let .failure(error):
                completion(.failure(error));
            }
        }
//        completion(.success(identifier == "00000000"))
        
    }
    
    func randomInt() -> String {
        let randomInt = Int(arc4random_uniform(UInt32(10)));
        return Data(String(randomInt).utf8).base64EncodedString();
    }
    
    func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      let random = String((0..<length).map{ _ in letters.randomElement()! })
      return random;
    }
    
    @available(iOS 13.0, *)
    func verifySignature(destinationFileUrl: URL) -> Bool {
        do {
            let zipFile = FileManager.default.contents(atPath: destinationFileUrl.path)!
            do {
                guard let archive = Archive(data: zipFile, accessMode: .read) else {
                    return false
                }
                do {
                    let signatureFile = try archive.extractData(from: archive["export.sig"]!);
                    let binFile = try archive.extractData(from: archive["export.bin"]!);
                    let parsedSignatureFile = try TEKSignatureList(serializedData: signatureFile);
                    let tekSignature: TEKSignature = parsedSignatureFile.signatures[0];
                    
                    let signature = try P256.Signing.ECDSASignature(derRepresentation: tekSignature.signature)

                    do {
                        let pbKey = try P256.Signing.PublicKey(rawRepresentation: Data(base64Encoded: PUBLIC_KEY)!)
                        if pbKey.isValidSignature(signature, for: binFile) {
                            print("File verified")
                            return true
                        } else {
                            print("File verification failed")
                        }
                    } catch {
                        print("Could not initialize public key: \(error.localizedDescription)")
                    }

                    return false // no match at all
                } catch {
                    print("Could not extract key package: \(error.localizedDescription)");
                }
                
            } catch {
                print("verification error! \(error.localizedDescription)")
                return false
            }
        } catch {
            print("Could not read data: \(error.localizedDescription)")
        }
        
        return false;
    }
    
}

struct VerifyCodeResult: Codable {
    var padding: String?
    var testtype: String?
    var symptomDate: String?
    var testDate: String?
    var token: String?
    var error: String?
    var errorCode: String?
    var error_code: String?
}

struct VerificationCertResult: Codable {
    var padding: String?
    var certificate: String?
    var error: String?
    var errorCode: String?
    var error_code: String?
}

struct UploadResult: Codable {
    var revisionToken: String?
    var insertedExposures: Int?
    var padding: String?
    var error: String?
    var code: String?
}

struct UploadData: Codable {
    var temporaryExposureKeys: [ENTek]
    var healthAuthorityID: String
    var verificationPayload: String?
    var hmacKey: String
    var symptomOnsetInterval: UInt32?
    var traveler: Bool?
    var revisionToken: String
    var padding: String
}

struct ENTek : Codable {
    var key: String
    var rollingPeriod: ENIntervalNumber
    var rollingStartNumber: ENIntervalNumber
    var transmissionRisk: ENRiskLevel
}

class ENThreshold : NSObject {
    var confirmedTestPerDaySumERVThreshold: Double = 0.0
    var clinicalDiagnosisPerDaySumERVThreshold: Double = 0.0
    var selfReportPerDaySumERVThreshold: Double = 0.0
    var recursivePerDaySumERVThreshold: Double = 0.0
    var perDaySumERVThreshold: Double = 0.0
    var perDayMaxERVThreshold: Double = 0.0
    var weightedDurationAtAttenuationThreshold: Double = 0.0
}

class ENConfig: NSObject {
    var exposureConfig: ENExposureConfiguration? = nil
    var threshold: ENThreshold? = nil
}

extension Archive {
   
    func extractData(from entry: Entry) throws -> Data {
        var data = Data()
        try _ = extract(entry) { slice in
            data.append(slice)
        }
        return data
    }

}

