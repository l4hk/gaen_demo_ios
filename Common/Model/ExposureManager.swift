/*
See LICENSE folder for this sample’s licensing information.

Abstract:
A class that manages a singleton ENManager object.
*/

import Foundation
import ExposureNotification
import UserNotifications

class ExposureManager {
    
    static let shared = ExposureManager()
    let userNotificationCenter = UNUserNotificationCenter.current()
    
    let manager = ENManager()
    
    init() {
        if #available(iOS 13.5, *) {
            // In iOS 13.5 and later, the Background Tasks framework is available,
            // so create and schedule a background task for downloading keys and
            // detecting exposures
            createBackgroundTaskIfNeeded()
            scheduleBackgroundTaskIfNeeded()
        } else if ENManagerIsAvailable() {
            // If `ENManager` exists, and the iOS version is earlier than 13.5,
            // the app is running on iOS 12.5, where the Background Tasks
            // framework is unavailable. Specify an EN activity handler here, which
            // allows the app to receive background time for downloading keys
            // and looking for exposures when background tasks aren't available.
            // Apps should should call this method before calling activate().
            manager.setLaunchActivityHandler { (activityFlags) in
                // ENManager gives apps that register an activity handler
                // in iOS 12.5 up to 3.5 minutes of background time at
                // least once per day. In iOS 13 and later, registering an
                // activity handler does nothing.
                if activityFlags.contains(.periodicRun) {
                    print("Periodic activity callback called (iOS 12.5)")
                    _ = ExposureManager.shared.detectExposures()
                }
            }
        }
        print("appInstallTs", LocalStore.shared.appInstallTs )
        if (LocalStore.shared.appInstallTs == 0.0) {
            LocalStore.shared.appInstallTs = Date().timeIntervalSince1970;
            print("Set app install ts", LocalStore.shared.appInstallTs )
        }
        manager.activate { _ in
            // Ensure Exposure Notifications is enabled if the app is authorized. The app
            // could get into a state where it is authorized, but Exposure Notifications
            // is not enabled, if the user initially denied Exposure Notifications
            // during onboarding, but then flipped on the "COVID-19 Exposure Notifications" switch
            // in Settings.
            print("self.manager.exposureNotificationEnabled:", self.manager.exposureNotificationEnabled)
            print("ENManager.authorizationStatus:", ENManager.authorizationStatus.rawValue);
            print("manager.exposureNotificationStatus:", self.manager.exposureNotificationStatus.rawValue);
            if !self.manager.exposureNotificationEnabled {
                if !LocalStore.shared.isUserDisabled {
                    self.manager.setExposureNotificationEnabled(true) { (error) in
                        if let error = error {
                            print("Error attempting to enable on launch: \(error.localizedDescription)")
                        }
                    }
                }
            }
        }
    }
    deinit {
        manager.invalidate()
    }

    static let authorizationStatusChangeNotification = Notification.Name("ExposureManagerAuthorizationStatusChangedNotification")
    
    var detectingExposures = false
    
    func detectExposures(completionHandler: ((Bool) -> Void)? = nil) -> Progress {
        
        let progress = Progress()
        LocalStore.shared.exposureDetectionErrorLocalizedDescription = nil;
        // Disallow concurrent exposure detection, because if allowed we might try to detect the same diagnosis keys more than once
        guard !detectingExposures else {
            completionHandler?(false)
            return progress
        }
        detectingExposures = true
        
        var localURLs = [URL]()
        var remoteUrlCount = 0;
        
        func finish(_ result: Result<([Exposure], Int), Error>) {
            
            try? Server.shared.deleteDiagnosisKeyFile(at: localURLs)
            
            let success: Bool
            if progress.isCancelled {
                success = false
            } else {
                switch result {
                case let .success((newExposures, nextDiagnosisKeyFileIndex)):
//                    print("nextDiagnosisKeyFileIndex: ", nextDiagnosisKeyFileIndex);
//                    LocalStore.shared.nextDiagnosisKeyFileIndex = nextDiagnosisKeyFileIndex
                    LocalStore.shared.nextDiagnosisKeyFile = LocalStore.shared.tmpNextDiagnosisKeyFile
                    print("nextDiagnosisKeyFile: ", LocalStore.shared.nextDiagnosisKeyFile);
                    // Starting with the V2 API, ENManager will return cached results, so replace our saved results instead of appending
                    if #available(iOS 13.7, *) {
                        LocalStore.shared.exposures = newExposures
                    } else {
                        LocalStore.shared.exposures.append(contentsOf: newExposures)
                    }
                    print("[newExposures]", newExposures.count)
                    if (newExposures.count > 0) {
                        self.sendNotification();
                    }
                    LocalStore.shared.exposures.sort { $0.date < $1.date }
                    LocalStore.shared.dateLastPerformedExposureDetection = Date()
                    LocalStore.shared.exposureDetectionErrorLocalizedDescription = nil
                    success = true
                case let .failure(error):
                    LocalStore.shared.exposureDetectionErrorLocalizedDescription = error.localizedDescription
                    // Consider posting a user notification that an error occured
                    success = false
                }
            }
            
            detectingExposures = false
            completionHandler?(success)
        }
        
        func checkExposureThreshold(conf: ENThreshold, ds: ENExposureDaySummary) -> Bool {
            return thresholdNonZero(threshold: conf.confirmedTestPerDaySumERVThreshold,
                                    summaryValue: ds.confirmedTestSummary?.scoreSum ?? 0)
                || thresholdNonZero(threshold: conf.clinicalDiagnosisPerDaySumERVThreshold,
                                    summaryValue: ds.confirmedClinicalDiagnosisSummary?.scoreSum ?? 0)
                || thresholdNonZero(threshold: conf.selfReportPerDaySumERVThreshold,
                                    summaryValue: ds.selfReportedSummary?.scoreSum ?? 0)
                || thresholdNonZero(threshold: conf.recursivePerDaySumERVThreshold,
                                    summaryValue: ds.recursiveSummary?.scoreSum ?? 0)
                || thresholdNonZero(threshold: conf.perDaySumERVThreshold,
                                    summaryValue: ds.daySummary.scoreSum)
                || thresholdNonZero(threshold: conf.perDayMaxERVThreshold,
                                    summaryValue: ds.daySummary.maximumScore)
                || thresholdNonZero(threshold: conf.weightedDurationAtAttenuationThreshold,
                                    summaryValue:                                   ds.daySummary.weightedDurationSum);
        }
        
        func thresholdNonZero(threshold: Double, summaryValue: Double) -> Bool {
            return threshold != 0 && summaryValue > threshold;
        }
        
        func convertSummary(item: ENExposureSummaryItem?) -> DaySummary? {
            if item != nil {
                return DaySummary(scoreSum: item!.scoreSum,
                           maximumScore: item!.maximumScore,
                           weightedDurationSum: item!.weightedDurationSum);
            } else {
                return nil
            }
        }
        
        // Handles getting exposures using the version 2 API in iOS 13.7+ and
        // in iOS 12.5
        func getExposuresV2(_ summary: ENExposureDetectionSummary, configuration: ENThreshold) {
            print("start getExposuresV2 summary", summary)
           
            var exposureList: [Exposure] = [];
            var showAlert = false;
            
            let strDuration = summary.attenuationDurations.map {
              (number: NSNumber) -> String in
                
                return number.stringValue
            }
           
            var strAttDuration = "N/A"
            if !strDuration.isEmpty {
                strAttDuration = strDuration.joined(separator: ",");
            }
            let attDuration: String = "Attenuation Duration: " + strAttDuration + "\n";
            let daysExposure: String = "Days Since Last Exposure: " + String(summary.daysSinceLastExposure) + "\n";
            let matchedKey: String = "Matched Key Count: " + String(summary.matchedKeyCount) + "\n";
            let maxRiskScore: String = "Max Risk Score: " + String(summary.maximumRiskScore) + "\n";
            let maxRiskScoreFull: String = "Max Risk Score Full Range: " + String(summary.maximumRiskScoreFullRange) + "\n";
            let riskSumFullRange: String = "Risk Score Sum Full Range: " + String(summary.riskScoreSumFullRange) + "\n";
            let summaryInfo: String = attDuration
                                    .appending(daysExposure)
                                    .appending(matchedKey)
                                    .appending(maxRiskScore)
                                    .appending(maxRiskScoreFull)
                                    .appending(riskSumFullRange);
            print("start getExposuresV2 summary", summary)
            
            var exposureWindow: [ENExposureWindow] = [];
            self.manager.getExposureWindows(summary: summary) { windows, error in
                if let error = error {
                    print("getExposureWindows error", error)
                }
                
                let allWindows = windows!.map { window in
                    exposureWindow.append(window);
                }
                 
                for daySummary in summary.daySummaries {
                    if checkExposureThreshold(conf: configuration, ds: daySummary) {
                        print("is over threshold")
                        showAlert = true;
                    } else {
                        showAlert = false;
                        print("not over threshold, daySummary:", daySummary);
                    }
                    let filteredItems = exposureWindow.filter { item in return item.date == daySummary.date }
                    let strExposureWindow = convertStrExposureWindow(windows: filteredItems);
                   
                    exposureList.append(Exposure(
                    date: daySummary.date,
                    summaryDetails: summaryInfo,
                    confirmedClinicalDiagnosisSummary: convertSummary(item: daySummary.confirmedClinicalDiagnosisSummary),
                    confirmedTestSummary: convertSummary(item: daySummary.confirmedTestSummary),
                    recursiveSummary: convertSummary(item: daySummary.recursiveSummary),
                    selfReportedSummary: convertSummary(item: daySummary.selfReportedSummary),
                    daySummary: convertSummary(item: daySummary.daySummary),
                    strExposureWindow: strExposureWindow,
                    showAlert: showAlert));
                }
                finish(.success((exposureList, nextDiagnosisKeyFileIndex + remoteUrlCount)))
            }
        }
        
        func getCalibrationConfidence(confidence:ENCalibrationConfidence ) -> String {
            var calibrationConfidence = "Calibration Confidence: ";
            switch confidence {
            case ENCalibrationConfidence.high:
                calibrationConfidence.append("High\n");
            case ENCalibrationConfidence.medium:
                calibrationConfidence.append("Medium\n");
            case ENCalibrationConfidence.low:
                calibrationConfidence.append("Low\n");
            case ENCalibrationConfidence.lowest:
                calibrationConfidence.append("Lowest\n");
            default:
                calibrationConfidence.append("Unknown\n");
            }
            return calibrationConfidence;
        }
        
        func getReportType(type: ENDiagnosisReportType) -> String {
            var reportType = "Diagnosis Report Type: ";
            switch type {
            case ENDiagnosisReportType.confirmedClinicalDiagnosis:
                reportType.append("Confirmed Clinical Diagnosis\n");
            case ENDiagnosisReportType.confirmedTest:
                reportType.append("Confirmed Test\n");
            case ENDiagnosisReportType.recursive:
                reportType.append("Recursive\n");
            case ENDiagnosisReportType.revoked:
                reportType.append("Revoked\n");
            case ENDiagnosisReportType.selfReported:
                reportType.append("Self Reported\n");
            case ENDiagnosisReportType.unknown:
                reportType.append("Unknown\n");
            default:
                reportType.append("Unknown\n");
            }
            return reportType;
        }
        
        func getInfectiousness(value: ENInfectiousness) -> String {
            var infectiousness = "Infectiousness: ";
            switch value {
            case ENInfectiousness.high:
                infectiousness.append("High\n");
            case ENInfectiousness.standard:
                infectiousness.append("Standard\n");
            case ENInfectiousness.none:
                infectiousness.append("None\n");
            default:
                infectiousness.append("Unknown\n");
            }
            return infectiousness;
        }
        
        func convertScanInstance(scanInstances: [ENScanInstance]) -> String {
            var strScanInstance = "Scan Instances:\n";
            for (index, element) in scanInstances.enumerated() {
                let minimumAttenuation = "Minimum Attenuation: ".appending(String(element.minimumAttenuation)).appending("\n");
                let secondsSinceLastScan = "Seconds Since Last Scan: ".appending(String(element.secondsSinceLastScan)).appending("\n");
                let typicalAttenuation = "Typical Attenuation: ".appending(String(element.typicalAttenuation)).appending("\n");
                strScanInstance = strScanInstance.appending("------------------------------\n")
                    .appending("Instance #\(index+1)\n")
                    .appending(minimumAttenuation)
                    .appending(secondsSinceLastScan)
                    .appending(typicalAttenuation)
            }
            
            return strScanInstance;
        }
        
        func convertStrExposureWindow(windows: [ENExposureWindow]) -> String {
            var strExpWindowList = "Exposure Windows:\n";
            for (index, window) in windows.enumerated() {
                let calibrationConfidence = getCalibrationConfidence(confidence: window.calibrationConfidence);
                let diagnosisReportType = getReportType(type: window.diagnosisReportType);
                let date = "Date: " + DateFormatter.localizedString(from: window.date, dateStyle: .long, timeStyle: .none) + "\n";
                let infectiousness = getInfectiousness(value: window.infectiousness);
                let scanInstance = convertScanInstance(scanInstances: window.scanInstances);
                let exposureWindow = date.appending(calibrationConfidence).appending(diagnosisReportType).appending(infectiousness).appending(scanInstance);
                strExpWindowList = strExpWindowList.appending("------------------------------\n")
                    .appending("Window \(index + 1):\n\(exposureWindow)");
            }
            return strExpWindowList;
//           var scanInstances: [ENScanInstance]

        }
        
        
        // Handles getting exposures using the version 1 API used in iOS 13.5 and iOS 13.6
        @available(iOS 13.5, *)
        func getExposuresV1(_ summary: ENExposureDetectionSummary) {
            let userExplanation = NSLocalizedString("USER_NOTIFICATION_EXPLANATION", comment: "User notification")
            ExposureManager.shared.manager.getExposureInfo(summary: summary,
                                                           userExplanation: userExplanation) { exposures, error in
                if let error = error {
                    finish(.failure(error))
                    return
                }
                let newExposures = exposures!.map { exposure in
                    Exposure(date: exposure.date, showAlert: false)
                }
                finish(.success((newExposures, nextDiagnosisKeyFileIndex + remoteUrlCount)))
            }
        }
        
        let nextDiagnosisKeyFileIndex = LocalStore.shared.nextDiagnosisKeyFileIndex
        
        Server.shared.getDiagnosisKeyFileURLs(startingAt: nextDiagnosisKeyFileIndex) { result in
            print("result:", result);
            let dispatchGroup = DispatchGroup()
            var localURLResults = [Result<[URL], Error>]()
            var checkExposure = true;
            switch result {
            case let .success(remoteURLs):
                remoteUrlCount = remoteURLs.count;
                
//                if (nextDiagnosisKeyFileIndex == 0) {
//                    nextDiagnosisKeyFileIndex = LocalStore.shared.tmpNextDiagnosisKeyFileIndex
//                }
                
                for remoteURL in remoteURLs {
                    dispatchGroup.enter()
                    Server.shared.downloadDiagnosisKeyFile(at: remoteURL) { result in
                        localURLResults.append(result)
                        dispatchGroup.leave()
                    }
                }
                
            case let .failure(error):
                checkExposure = false;
                DispatchQueue.main.async {
                    print("[getDiagnosisKeyFileURLs] failure:", error);
                    finish(.failure(error))
                }
            }
            
            if checkExposure {
                dispatchGroup.notify(queue: .main) {
                    for result in localURLResults {
                        switch result {
                        case let .success(urls):
                            localURLs.append(contentsOf: urls)
                        case let .failure(error):
                            finish(.failure(error))
                            return
                        }
                    }
                    print("localURLs:", localURLs);
                    Server.shared.getExposureConfiguration { result in
                        switch result {
                        case let .success(configuration):
                            print("call detectExposures");
                            ExposureManager.shared.manager.detectExposures(configuration: configuration.exposureConfig!, diagnosisKeyURLs: localURLs) { summary, error in
                                print("detectExposures result: ", summary);
                                if let error = error {
                                    print("call detectExposures error1", error);
                                    finish(.failure(error))
                                    return
                                }
                                if #available(iOS 13.7, *) {
                                    getExposuresV2(summary!, configuration: configuration.threshold!)
                                } else if #available(iOS 13.5, *) {
                                    getExposuresV1(summary!)
                                } else if ENManagerIsAvailable() {
                                    getExposuresV2(summary!, configuration: configuration.threshold!)
                                } else {
                                    print("Exposure Notifications not supported on this version of iOS.")
                                }
                            }
                            
                        case let .failure(error):
                            print("call detectExposures error2", error);
                            finish(.failure(error))
                        }
                    }
                    
                }
            }
        }
        
        return progress
    }

    func getAndPostDiagnosisKeys(testResult: TestResult, token: String, symptomDate: UInt32, completion: @escaping (Error?) -> Void) { //
//        manager.getDiagnosisKeys { temporaryExposureKeys, error in
        manager.getTestDiagnosisKeys { temporaryExposureKeys, error in
            if let error = error {
                completion(error)
            } else {
                guard let temporaryExposureKeys = temporaryExposureKeys else {
                    print("No exposure keys, aborting key share")
                    return
                }
                
                // In this sample app, transmissionRiskLevel isn't set for any of the diagnosis keys. However, it is at this point that an app could
                // use information accumulated in testResult to determine a transmissionRiskLevel for each diagnosis key.
                Server.shared.postDiagnosisKeys(temporaryExposureKeys, token: token, symptomDate: symptomDate) { error in
                    completion(error)
                }
            }
        }
    }
    
    // Includes today's key, requires com.apple.developer.exposure-notification-test entitlement
    func getAndPostTestDiagnosisKeys(completion: @escaping (Error?) -> Void) {
        manager.getTestDiagnosisKeys { temporaryExposureKeys, error in
            if let error = error {
                completion(error)
            } else {
                Server.shared.postDiagnosisKeys(temporaryExposureKeys!, token: "", symptomDate: 0) { error in
                    completion(error)
                }
            }
        }
    }
    
    func showBluetoothOffUserNotificationIfNeeded() {
        let identifier = "bluetooth-off"
        if ENManager.authorizationStatus == .authorized && manager.exposureNotificationStatus == .bluetoothOff {
            let content = UNMutableNotificationContent()
            content.title = NSLocalizedString("USER_NOTIFICATION_BLUETOOTH_OFF_TITLE", comment: "User notification title")
            content.body = NSLocalizedString("USER_NOTIFICATION_BLUETOOTH_OFF_BODY", comment: "User notification")
            content.sound = .default
            let request = UNNotificationRequest(identifier: identifier, content: content, trigger: nil)
            UNUserNotificationCenter.current().add(request) { error in
                DispatchQueue.main.async {
                    if let error = error {
                        print("Error showing error user notification: \(error)")
                    }
                }
            }
        } else {
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [identifier])
        }
    }

    @available(iOS 14.4, *)
    func preAuthorizeKeys(
        completion: @escaping (Error?) -> Void) {
        manager.preAuthorizeDiagnosisKeys { (error) in
            if let error = error {
                print("Error pre-authorizing keys: \(error)")
                completion(error)
                return
            }
            print("Successfully pre-authorized keys")
            completion(nil)
        }
    }
    
    @available(iOS 14.4, *)
    func requestAndPostPreAuthorizedKeys(
        completion: @escaping (Error?) -> Void) {
        // This handler receives preauthorized keys. Once the handler is called,
        // the preauthorization expires, so the handler should only be called
        // once per preauthorization request. If the user doesn't authorize
        // release, this handler isn't called.
        manager.diagnosisKeysAvailableHandler = { (keys) in
            Server.shared.postDiagnosisKeys(keys, token: "", symptomDate: 0) { (error) in
                if let error = error {
                    print("Error posting pre-authorized diagnosis keys: \(error)")
                }
                completion(error)
            }
        }
        
        // This call requests preauthorized keys. The request fails if the
        // user doesn't authorize release or if more than five days pass after
        // authorization. If requestPreAuthorizedDiagnosisKeys(:) has already
        // been called since the last time the user preauthorized, the call
        // doesn't fail but also doesn't return any keys.
        manager.requestPreAuthorizedDiagnosisKeys { (error) in
            if let error = error {
                print("Error retrieving pre-authorized diganosis keys: \(error)")
                completion(error)
            }
        }
    }
    
    func sendNotification() {
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "Possible COVID-19 Notification"
        notificationContent.body = "You have visited a venue that a COVID-19 patient has also visited."
        notificationContent.badge = NSNumber(value: 1);
        
        let request = UNNotificationRequest(identifier: "closeContactNotif",
                                            content: notificationContent,
                                            trigger: nil)
        
        userNotificationCenter.add(request) { (error) in
            if let error = error {
                print("Notification Error: ", error)
            }
        }
    }
    
}
