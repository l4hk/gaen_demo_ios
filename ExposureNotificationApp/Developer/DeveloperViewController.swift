/*
See LICENSE folder for this sample’s licensing information.

Abstract:
A view controller used in developer builds to simulate various app behaviors.
*/

import UIKit
import ExposureNotification

class DeveloperViewController: UITableViewController, UNUserNotificationCenterDelegate {
    
    enum Section: Int {
        case general
    }
    
    enum GeneralRow: Int {
        case detectExposuresNow
        case enableExposureNotifications
        case disableExposureNotifications
        case resetLocalExposures
        case resetLocalTestResults
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

           //If you don't want to show notification when app is open, do something here else and make a return here.
           //Even you you don't implement this delegate method, you will not see the notification on the specified controller. So, you have to implement this delegate and make sure the below line execute. i.e. completionHandler.

           completionHandler([.alert, .badge, .sound])
       }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch Section(rawValue: indexPath.section)! {
        case .general:
            switch GeneralRow(rawValue: indexPath.row)! {
                
            case .detectExposuresNow:
                _ = ExposureManager.shared.detectExposures()
                
            case .enableExposureNotifications:
                LocalStore.shared.isUserDisabled = false
                ExposureManager.shared.manager.setExposureNotificationEnabled(true) { error in
                    if let error = error {
                        showError(error, from: self)
                    }
                }
                
            case .disableExposureNotifications:
                LocalStore.shared.isUserDisabled = true
                ExposureManager.shared.manager.setExposureNotificationEnabled(false) { error in
                    if let error = error {
                        showError(error, from: self)
                    }
                }
                
            case .resetLocalExposures:
                LocalStore.shared.tmpNextDiagnosisKeyFile = nil
                LocalStore.shared.nextDiagnosisKeyFile = nil
//                LocalStore.shared.nextDiagnosisKeyFileIndex = 0
                LocalStore.shared.exposures = []
                LocalStore.shared.dateLastPerformedExposureDetection = nil
                
            case .resetLocalTestResults:
                LocalStore.shared.testResults = [:]
            }
        }
    }
}
