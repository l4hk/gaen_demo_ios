//
//  ShareDiagnosisKeysViewController.swift
//  ExposureNotificationApp-iOS12
//
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class ShareDiagnosisKeysViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtVerificationCode: UITextField!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Share Diagnosis Keys"
        txtVerificationCode.delegate = self;
    }
    
    
    @IBAction func btnNextClick(_ sender: Any) {
        verifyCode();
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      
        if textField == txtVerificationCode {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
    
    func verifyCode() {
        if self.txtVerificationCode.text!.isEmpty {
            print ("code is empty")
            DispatchQueue.main.async {
                showError(NSError(domain:"", code:401, userInfo:[ NSLocalizedDescriptionKey: "Please enter Verification code"]), from: self)
            }
        } else {
            Server.shared.verifyUniqueTestIdentifier(self.txtVerificationCode.text!) { result in
            switch result {
                case let .success(response):
                    if (response.token != nil && !(response.token?.isEmpty ?? true)) {
            //                                                if response == "true" {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            print ("success",response.token);
//                            var testAdminController = TestAdministrationDateViewController.make(testResultID: self.testResultID);
//                            testAdminController.token = response.token ?? "";
//                            self.show(testAdminController, sender: nil)
                            var testResult = TestResult(id: UUID(), isAdded: false, dateAdministered: Date(), isShared: false)
                            ExposureManager.shared.getAndPostDiagnosisKeys(testResult: testResult, token: response.token!) { error in
                                if let error = error {
                                    DispatchQueue.main.async {
                                        showError(error, from: self)
                                    }
                                } else {
                                    DispatchQueue.main.async {
                                        testResult.isShared = true
                                        let alert = UIAlertController(title: NSLocalizedString("SUCCESS", comment: "Title"), message: "Upload completed", preferredStyle: .alert)
                                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Button"), style: .cancel))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                }
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(
                                title: NSLocalizedString("VERIFICATION_IDENTIFIER_INVALID", comment: "Alert title"),
                                message: response.error,
                                preferredStyle: .alert
                            )
                            alertController.addAction(.init(title: NSLocalizedString("OK", comment: "Button"),
                                                            style: .cancel, handler: nil))
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                case let .failure(error):
                    DispatchQueue.main.async {
                        showError(error, from: self)
                    }
                }
            }
        }
    }
}
